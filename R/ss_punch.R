#' Punch Data and Details
#' 
#' Returns Unit Details and Hierarchy
#' @export
#' @importFrom magrittr %>%
#' @author Matt Dickinson / Joel Bugayong
#' @return A data table of the time and attendence data from Smart Square (shr_time_punch_summ) with associated detaisl
#' @param oem_id The Smart Square OEM
#' @param start_date The earliest pay period end date you wish to pull in yyyy-mm-dd format. Note, if not an actual end date it will pull starting at the next pay period end date
#' @param end_date The latest pay period end date you wish to pull in yyyy-mm-dd format. Note, if not an actual end date it will stop at the previous pay period end date
#' @param skill_type The skill category type. Must be either STAFF, FIN, or RPT (the default is STAFF)

ss_punch <- function(oem_id, start_date, end_date, skill_type = 'STAFF') {
  
  skill_type <- toupper(skill_type)
  
  start_date <- if(!is(as.Date(start_date, format = "%Y-%m-%d"), "Date")){stop('Start Date must be a valid date format')}
  else {as.Date(start_date, format = "%Y-%m-%d")}
  
  end_date <- if(!is(as.Date(end_date, format = "%Y-%m-%d"), "Date")){stop('Start Date must be a valid date format')}
  else {as.Date(end_date, format = "%Y-%m-%d")}
  
  if (skill_type %in% c('STAFF','FIN','RPT')) {skill_type} 
  else {stop('Skill type must be either STAFF or FIN or RPT')}
  
  query <- paste0("
                  WITH CC AS (
                  SELECT
                  cc.oem_id
                  ,systems.sch_system_id
                  ,facilities.sch_system_facility_id
                  ,cc.sch_cost_center_id
                  ,peer_cat.sch_peer_group_cat_id
                  ,service_lines.sch_service_line_id
                  ,systems.system_name
                  ,systems.system_abbr
                  ,facilities.facility_name
                  ,facilities.facility_abbr
                  ,cc.cost_center_name
                  ,cc.cost_center_abbr
                  ,peer_cat.peer_group_cat_name
                  ,peer_cat.peer_group_cat_abbr
                  ,service_lines.service_line_name
                  ,service_lines.service_line_abbr
                  
                  FROM sch.sch_cost_centers cc
                  
                  LEFT JOIN sch.sch_system_facility facilities
                  ON cc.sch_system_facility_id = facilities.sch_system_facility_id
                  
                  LEFT JOIN sch.sch_system systems
                  ON facilities.sch_system_id = systems.sch_system_id
                  
                  LEFT JOIN sch.sch_peer_group_cat peer_cat
                  ON cc.sch_peer_group_cat_id = peer_cat.sch_peer_group_cat_id
                  
                  LEFT JOIN sch.sch_service_lines service_lines
                  ON peer_cat.sch_service_line_id = service_lines.sch_service_line_id
                  )
                  ,SKILLS AS (
                  SELECT
                  skills.sch_skill_id
                  ,skill_cat.sch_skill_category_id
                  ,sl_cat.sch_service_line_id
                  ,sl.service_line_name
                  ,skills.skill_name
                  ,skills.skill_abbr
                  ,skill_cat.skill_category_name
                  ,skill_cat.skill_category_abbr
                  ,sl_cat.skill_cat_type
                  
                  FROM sch.sch_skills skills
                  
                  INNER JOIN sch.sch_skill_categories skill_cat_link
                  ON skills.sch_skill_id = skill_cat_link.sch_skill_id
                  
                  INNER JOIN sch.sch_skill_category skill_cat
                  ON skill_cat_link.sch_skill_category_id = skill_cat.sch_skill_category_id
                  
                  INNER JOIN sch.sch_service_line_skill_cat sl_cat
                  ON skill_cat.sch_skill_category_id = sl_cat.sch_skill_category_id
                  
                  INNER JOIN sch.sch_service_lines sl
                  ON sl_cat.sch_service_line_id = sl.sch_service_line_id
                  
                  WHERE sl_cat.skill_cat_type = '", skill_type , "'
                  )
                  SELECT
                  punch.oem_id
                  ,punch.sch_cost_center_id           worked_sch_cost_center_id
                  ,worked_cc.cost_center_name         worked_cost_center_name
                  ,worked_cc.cost_center_abbr         worked_cost_center_abbr
                  ,worked_cc.sch_system_facility_id   worked_sch_system_facility_id
                  ,worked_cc.facility_name            worked_facility_name
                  ,worked_cc.facility_abbr            worked_facility_abbr
                  ,worked_cc.sch_peer_group_cat_id    worked_sch_peer_group_cat_id
                  ,worked_cc.peer_group_cat_name      worked_peer_group_cat_name
                  ,worked_cc.peer_group_cat_abbr      worked_peer_group_cat_abbr
                  ,worked_cc.sch_service_line_id      worked_sch_service_line_id
                  ,worked_cc.service_line_name        worked_service_line_name
                  ,worked_cc.service_line_abbr        worked_service_line_abbr
                  
                  ,punch.primary_cost_center_id       home_sch_cost_center_id
                  ,home_cc.cost_center_name           home_cost_center_name
                  ,home_cc.cost_center_abbr           home_cost_center_abbr
                  ,home_cc.sch_system_facility_id     home_sch_system_facility_id
                  ,home_cc.facility_name              home_facility_name
                  ,home_cc.facility_abbr              home_facility_abbr
                  ,home_cc.sch_peer_group_cat_id      home_sch_peer_group_cat_id
                  ,home_cc.peer_group_cat_name        home_peer_group_cat_name
                  ,home_cc.peer_group_cat_abbr        home_peer_group_cat_abbr
                  ,home_cc.sch_service_line_id        home_sch_service_line_id
                  ,home_cc.service_line_name          home_service_line_name
                  ,home_cc.service_line_abbr          home_service_line_abbr
                  
                  ,punch.account_id
                  ,account.first_name
                  ,account.last_name
                  ,punch.primary_skill_id
                  ,skills.skill_name
                  ,skills.skill_category_name         worked_skill_category_name
                  ,skills.skill_category_abbr         worked_skill_category_abbr
                  
                  ,punch.schedule_date
                  ,payperiod.pay_period_start_date
                  ,payperiod.pay_period_end_date
                  
                  ,punch.total_hours
                  ,punch.total_cost
                  ,punch.shr_pay_code_id
                  ,paycode.pay_code_name
                  ,paycode.productive_category
                  ,paycode.reporting_category
                  ,paycode.count_toward_hours
                  ,paycode.count_toward_fte
                  ,paycode.count_toward_cost
                  ,paycode.count_as_overtime
                  ,paycode.is_oncall_paycode
                  
                  FROM shr_time_punch_summ punch
                  
                  LEFT JOIN CC worked_cc
                  ON worked_cc.sch_cost_center_id = punch.sch_cost_center_id
                  
                  LEFT JOIN CC home_cc
                  ON home_cc.sch_cost_center_id = punch.primary_cost_center_id
                  
                  INNER JOIN pc_account account
                  ON account.account_id = punch.account_id
                  
                  INNER JOIN SKILLS
                  ON SKILLS.sch_skill_id = punch.primary_skill_id
                  AND SKILLS.sch_service_line_id = worked_cc.sch_service_line_id
                  
                  INNER JOIN shr_tc_payperiods payperiod
                  ON payperiod.shr_payperiod_id = punch.shr_payperiod_id
                  
                  INNER JOIN shr_pay_codes paycode
                  ON paycode.shr_pay_code_id = punch.shr_pay_code_id
                  
                  WHERE punch.oem_id = " ,oem_id,"
                  AND payperiod.pay_period_end_date BETWEEN TO_DATE('",start_date,"', 'yyyy-mm-dd') AND TO_DATE('",end_date,"', 'yyyy-mm-dd')"
  )
  
  result <- dtQuerySmartSquare(query)
  
  return(result)
}